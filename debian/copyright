Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xmount
Upstream-Contact: Gillen Daniel <development@sits.lu>
Source: https://code.sits.lu/foss/xmount
Files-Excluded:
 libxmount_input/libxmount_input_aff/*.tar.gz
 libxmount_input/libxmount_input_aff4/*.tar.gz
 libxmount_input/libxmount_input_ewf/*.tar.gz

Files: *
Copyright: 2008-2024 Gillen Daniel
License: GPL-3+

Files: src/md5.c src/md5.h
Copyright: 1999 Aladdin Enterprises
License: Zlib
 Copyright (C) 1999 Aladdin Enterprises.  All rights reserved.
 .
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: debian/*
Copyright: 2009-2014 Michael Prokop <mika@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the Free
 Software Foundation, either version 3 of the License, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but WITHOUT
 ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 more details.
 .
 The full text of the GPL version 3 is distributed in
 /usr/share/common-licenses/GPL-3 on Debian systems.
